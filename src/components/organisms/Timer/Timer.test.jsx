import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { Timer } from './index'

describe('<Timer />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<Timer>Test</Timer>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
