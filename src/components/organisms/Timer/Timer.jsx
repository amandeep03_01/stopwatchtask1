import React, { useEffect, useState } from 'react';
import { TimerDisplay } from '../../atoms/TimerDisplay/TimerDisplay';
import { Heading } from '../../atoms/Heading/Heading';
import { Button } from '../../atoms/Button/Button';
import './styles.scss'

const Timer = () => {
  const [seconds, setSeconds] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [isRunning, setIsRunning] = useState(false);

  useEffect(() => {
    let timer;

    if (isRunning) {
      timer = setInterval(() => {
        setSeconds((prevSeconds) => {
          if (prevSeconds === 59) {
            setMinutes((prevMinutes) => prevMinutes + 1);
            return 0;
          } else {
            return prevSeconds + 1;
          }
        });
      }, 1000);
    }

    return () => clearInterval(timer);
  }, [isRunning, minutes, seconds]);

  const handleButtonClick = (action) => {
    if (action === 'start') {
      setIsRunning(true);
    } else if (action === 'stop') {
      setIsRunning(false);
    } else if (action === 'reset') {
      setIsRunning(false);
      setMinutes(0);
      setSeconds(0);
    }
  };

  return (
    <div className="timer">
      <div className="container">
        <div className="timer-container">
          <Heading/>
          <TimerDisplay minutes={minutes} seconds={seconds} />
          <Button label="Start" disabled={isRunning} onClick={() => handleButtonClick('start')} />
          <Button label="Stop" disabled={!isRunning} onClick={() => handleButtonClick('stop')} />
          <Button label="Reset" onClick={() => handleButtonClick('reset')} />
        </div>
      </div>
    </div>
  );
};

export default Timer;

