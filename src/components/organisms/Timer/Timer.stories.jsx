import { Timer } from './index';

export default {
  title: 'Timer',
  component: Timer,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <Timer {...args}>sample component</Timer>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'Timer'
}
