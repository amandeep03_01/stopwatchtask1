import React,{useState,useEffect} from 'react'
import { Heading } from '../../atoms/Heading/Heading'
import { TimerDisplay } from '../../atoms/TimerDisplay/TimerDisplay'
import { Button } from '../../atoms/Button/Button'
import './styles.scss'


export const TimerContainer = () => {

    return (
      <>
        <Heading />
        <TimerDisplay />
      </>
    )
  }

