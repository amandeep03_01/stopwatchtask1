import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { TimerContainer } from './index'

describe('<TimerContainer />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<TimerContainer>Test</TimerContainer>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
