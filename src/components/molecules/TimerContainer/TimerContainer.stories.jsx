import { TimerContainer } from './index';

export default {
  title: 'TimerContainer',
  component: TimerContainer,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <TimerContainer {...args}>sample component</TimerContainer>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'TimerContainer'
}
