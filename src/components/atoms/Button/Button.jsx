import React from 'react'
import './styles.scss'


export const Button = ({ onClick, disabled, label }) => (
  <button className={label} onClick={onClick} disabled={disabled}>
    {label}
  </button>
);
