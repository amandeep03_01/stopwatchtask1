import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { Button } from './index'

describe('<Button />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<Button>Test</Button>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
