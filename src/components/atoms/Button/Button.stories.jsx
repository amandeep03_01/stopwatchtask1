import { Button } from './index';

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <Button {...args}>sample component</Button>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'Button'
}
