import React, { useEffect, useState } from 'react';

export const Function = () =>{
    const [seconds, setSeconds] = useState(0);
    const [minutes, setMinutes] = useState(0);
    const [isRunning, setIsRunning] = useState(false);
  
    useEffect(() => {
      let timer;
  
      if (isRunning) {
        timer = setInterval(() => {
          setSeconds((prevSeconds) => {
            if (prevSeconds === 59) {
              setMinutes((prevMinutes) => prevMinutes + 1);
              return 0;
            } else {
              return prevSeconds + 1;
            }
          });
        }, 1000);
      }
  
      return () => clearInterval(timer);
    }, [isRunning, minutes, seconds]);
  
    const start = () => {
      setIsRunning(true);
    };
  
    const stop = () => {
      setIsRunning(false);
    };
  
    const reset = () => {
      setIsRunning(false);
      setMinutes(0);
      setSeconds(0);
    };

}
