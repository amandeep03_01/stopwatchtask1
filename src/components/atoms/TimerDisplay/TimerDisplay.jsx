import React,{useEffect,useState} from 'react'
import './styles.scss'

export const TimerDisplay = ({ minutes, seconds }) => {

  return (
    <h1>
      {minutes < 10 ? '0' + minutes : minutes}:{seconds < 10 ? '0' + seconds : seconds}
    </h1>
  )
};


