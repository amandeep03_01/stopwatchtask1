import { render } from '@testing-library/react'
import '@testing-library/jest-dom'

import { TimerDisplay } from './index'

describe('<TimerDisplay />', () => {
  test('should render correctly', () => {
    const { getByText } = render(<TimerDisplay>Test</TimerDisplay>)
    expect(getByText(/Test/i)).toBeInTheDocument()
  })
})
