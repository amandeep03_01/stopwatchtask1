import { TimerDisplay } from './index';

export default {
  title: 'TimerDisplay',
  component: TimerDisplay,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <TimerDisplay {...args}>sample component</TimerDisplay>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'TimerDisplay'
}
